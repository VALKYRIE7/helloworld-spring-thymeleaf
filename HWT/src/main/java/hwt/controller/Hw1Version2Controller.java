package hwt.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/*
 * Esta versión muestra hw1.html 
 * pero utilizando nombres de dirección diferentes a "hw1" 
*/
@Controller
public class Hw1Version2Controller {

	@GetMapping({"hw1v2", "hw1diferente"})
	public String pruebaHW1Version2() {
		System.out.println("Llamado desde Hw1Version2Controller");
		return "hw1";
	}
}
