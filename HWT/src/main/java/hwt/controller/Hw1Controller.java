package hwt.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class Hw1Controller {

	@GetMapping("/hw1")
	public String pruebaHW1() {
		System.out.println("Llamado desde Hw1Controller");
		return "hw1";
	}
}
