package hwt.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class Hw2Controller {

	@GetMapping("hw2")
	public String hw2Vista(Model model) {
		model.addAttribute("titulo", "Hello World desde Spring");
		return "hw2";
	}
}
